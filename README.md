# myRetail-Service

MyRetail-Service is an end-to-end Proof-of-Concept for a products API, which aggregates product data from multiple sources and returns it as JSON to the caller.

# Reference links

- [NoSQL data store - MongoDB](https://cloud.mongodb.com/v2/624e6fecfdd3e3143f15723b#metrics/replicaSet/624e70d7b72ef751bb51cf82/explorer/myretail-prices/prices/find)

## Dependencies

Install the following

- [Go version 1.14](https://go.dev/dl/)
- [Fiber](github.com/gofiber/fiber/v2)
- [MongoDB](go.mongodb.org/mongo-driver/mongo)
- [GoDotEnv](https://github.com/joho/godotenv)

# Getting started

First thing to do is clone down the repo then create a `.env` file with the following

```
MONGOURI=mongodb+srv://myRetail:w8uF3zb850qQJ08v@cluster0.ms6rm.mongodb.net/myRetail-prices?retryWrites=true&w=majority
DB=myretail-prices
PORT=6000
APP_ENV=development
```

## Run Service

```shell
go run main.go
```

## Postman Results

Example 1
![Alt text](Postman_myretail.png "Postman Result 1")

Example 2
![Alt text](Postman_myretail_2.png "Postman Result 2")

## Service

![Alt text](Service_Running.png "Service")
