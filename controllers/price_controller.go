package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/target-ms/myretail-service/configs"
	"gitlab.com/target-ms/myretail-service/models"

	"github.com/gofiber/fiber/v2"
	"go.mongodb.org/mongo-driver/bson"
)

// GetPriceAndProductInfo returns the price and product name for a given TCIN
func GetPriceAndProductInfo(c *fiber.Ctx) error {
	// price collection is data created in MongoDB
	priceCollection := configs.MI.DB.Collection("prices")
	//  timeout of 10 seconds when trying to connect
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	var price models.Price
	lookupTCIN := c.Params("tcin")
	filter := bson.D{{Key: "tcin", Value: lookupTCIN}}

	// finding the price for the given TCIN
	findResult := priceCollection.FindOne(ctx, filter)
	if err := findResult.Err(); err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"success": false,
			"message": "Price Not found",
			"error":   err,
		})
	}

	// unmarshal the price into price model
	err := findResult.Decode(&price)

	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"success": false,
			"message": "Price could not be decoded",
			"error":   err,
		})
	}

	// get the product name for the given TCIN
	productInfo, err := GetProductInfo(lookupTCIN)
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"success": false,
			"message": "Product Info Could not be found",
			"error":   err,
		})
	}

	// combines MongoDB price data with product name from Red Sky API
	priceProduct := models.PriceProduct{
		Price:       price.Price,
		ProductName: productInfo,
	}

	return c.Status(fiber.StatusOK).JSON(fiber.Map{
		"data":    priceProduct,
		"success": true,
	})
}

// GetProductInfo returns the product name for a given TCIN
func GetProductInfo(tcin string) (productName string, err error) {
	var productInfo models.ProductInfo
	baseUrl := "https://redsky-uat.perf.target.com/redsky_aggregations/v1/redsky/case_study_v1?key=3yUxt7WltYG7MFKPp7uyELi1K40ad2ys&tcin="
	response, err := http.Get(baseUrl + tcin)
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}

	// reads the response body
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}

	// unmarshal the response body into productInfo model
	err = json.Unmarshal(responseData, &productInfo)
	if err != nil {
		log.Fatal(err)
	}

	// set variable equal to the product name from the productInfo model
	productName = productInfo.Data.Product.Item.ProductDescription.Title

	return productName, nil

}
