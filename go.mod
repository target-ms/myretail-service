module gitlab.com/target-ms/myretail-service

go 1.14

require (
	github.com/go-playground/validator/v10 v10.10.1
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/gofiber/fiber/v2 v2.31.0
	github.com/golang/snappy v0.0.4 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/klauspost/compress v1.15.1 // indirect
	github.com/valyala/fasthttp v1.35.0 // indirect
	github.com/xdg-go/scram v1.1.1 // indirect
	github.com/youmark/pkcs8 v0.0.0-20201027041543-1326539a0a0a // indirect
	go.mongodb.org/mongo-driver v1.9.0
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220406163625-3f8b81556e12 // indirect
	gorm.io/gorm v1.23.4 // indirect
)
