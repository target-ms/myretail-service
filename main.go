package main

import (
	"log"
	"os"

	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"gitlab.com/target-ms/myretail-service/configs"
	"gitlab.com/target-ms/myretail-service/routes"
)

func main() {
	app := fiber.New()

	app.Use(cors.New())

	//run database
	configs.ConnectDB()

	// routes
	routes.PriceRoute(app)

	port := os.Getenv("PORT")

	err := app.Listen(":" + port)

	if err != nil {
		log.Fatal("Error app failed to start")
		panic(err)
	}
}
