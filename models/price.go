package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// Price represents the price of a product identified by its TCIN
type Price struct {
	Id                   primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	TargetCorpItemNumber string             `json:"tcin,omitempty" bson:"tcin,omitempty"`
	Price                string             `json:"price,omitempty" bson:"price,omitempty"`
}
