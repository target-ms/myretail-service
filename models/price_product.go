package models

// PriceProduct combines the Price and ProductName for a TCIN
type PriceProduct struct {
	Price       string
	ProductName string
}
