package models

// ProductInfo represents portion of product info from Red Sky API
type ProductInfo struct {
	Data struct {
		Product struct {
			TCIN string `json:"tcin"`
			Item struct {
				ProductDescription struct {
					Title string `json:"title"`
				} `json:"product_description"`
			} `json:"item"`
		} `json:"product"`
	} `json:"data"`
}
