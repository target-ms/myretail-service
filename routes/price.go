package routes

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/target-ms/myretail-service/controllers"
)

// PriceRoutes defines the routes for the price service
func PriceRoute(app *fiber.App) {
	app.Get("/prices/:tcin", controllers.GetPriceAndProductInfo)
}
